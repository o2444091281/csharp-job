﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetRoundArea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入圆的半径：（单位：cm）");
            Double r = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("该圆的面积为" + r * r * 3.14 + "平方厘米");
        }
    }
}
