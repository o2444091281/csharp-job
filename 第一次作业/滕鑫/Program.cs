﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            const double pi = 3.14;
            Console.WriteLine("请输入圆的半径：");
            int rad = int.Parse(Console.ReadLine());
            Console.WriteLine("半径圆的面积"+pi*rad*rad);
            Console.ReadKey();
        }
    }
}
