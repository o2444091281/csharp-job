﻿using System;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.ReadKey();
            Console.WriteLine("请输入圆形的半径");
            double r = double.Parse(System.Console.ReadLine());
            const double π = 3.14;
            double area = π * r * r;
            Console.WriteLine("圆形的面积是" + area);
        }
    }
}
        
