﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            double R;
            const double yzl = 3.14;
            Console.WriteLine("请输入圆的半径：");
            R = double.Parse(Console.ReadLine());
            double Area = yzl * R * R;
            Console.WriteLine("圆的面积为：" + Area);
        }
    }
}
