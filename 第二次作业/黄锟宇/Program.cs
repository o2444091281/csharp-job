﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class2
{
    class Program
    {
        //用户输入三个数，找出最大的数，打印输出。
        static void Main(string[] args)
        {
            word1();
            word2();
            word3();
            word4();
            word5();
        }
        static void word1()
        {
            //输入一行字符串，分别统计出其中英文字母、数字、空格的个数。
            Console.WriteLine("请输入一行字符串：");
            string arr = Console.ReadLine();
            int word = 0;
            int num = 0;
            int whritespace = 0;
            foreach (char item in arr)
            {
                if (char.IsLetter(item))
                {
                    word++;
                }
                else if (char.IsDigit(item))
                {
                    num++;
                }
                else if (char.IsWhiteSpace(item))
                {
                    whritespace++;
                }
            }
            Console.WriteLine("输入的一行字符串中有英文字母{0}个、数字{1}个、空格{2}个", word, num, whritespace);
        }
        static void word2()
        {
            //用户输入三个数，找出最大的数，打印输出。
            Console.WriteLine("请输入第一个数：");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数：");
            int num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第三个数：");
            int num3 = int.Parse(Console.ReadLine());
            int max;

            if (num1 > num2)
            {
                if (num1 > num3)
                {
                    max = num1;
                }
                else
                {
                    max = num3;
                }
            }
            else
            {
                if (num2 > num3)
                {
                    max = num2;
                }
                else
                {
                    max = num3;
                }
            }
            Console.WriteLine("最大的数为" + max);

        }
        static void word3()
        {
            /*
             编写一个程序，请用户输入一个四位整数，将用户输入的四位数的千位、百位、十位和个位分别显示出来，
                如5632，则显示“用户输入的千位为5，百位为6，十位为3，个位为2”
             */
            Console.WriteLine("请输入一个四位整数：");
            int num = int.Parse(Console.ReadLine());
            int qianwei = (num / 1000) % 10;
            int baiwei = (num / 100) % 10;
            int shiwei = (num / 10) % 10;
            int gewei = (num / 1) % 10;
            Console.WriteLine("用户输入的千位为" + qianwei + " ," + "百位为" + baiwei + " ," + "十位为" + shiwei + " , " + "各位" + gewei);
        }
        static void word4()
        {
            /*
             在 Main 方法中创建一个 double 类型的数组，并在该数组中存入 5 名学生的考试成绩，计算总成绩和平均成绩。
              要求使用foreach语句实现该功能，
             */
            double[] arr = new double[5];
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "名同学的考试成绩");
                arr[i] = double.Parse(Console.ReadLine());
            }
            double sum = 0;
            double avg = 0;
            foreach (var item in arr)
            {
                sum = sum + item;
                avg = sum / 5;
            }
            Console.WriteLine("总成绩为：" + sum);
            Console.WriteLine("平均成绩为：" + avg);
        }

        static void word5()
        {
            //定义一个方法，实现一维数组的排序功能，从大到小排序。
            int[] arr = new int[5];
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "个数");
                arr[i] = int.Parse(Console.ReadLine());
                return5(arr);
            }
            Console.Write("从大到小排序为");
            foreach (var item in arr)
            {
                Console.Write(item + " ");
            }

        }
        static void return5(int[] arr)
        {
            Array.Sort(arr);
            Array.Reverse(arr);

        }
    }
}