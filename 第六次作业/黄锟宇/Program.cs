﻿﻿using ConsoleApp1.ConsoleApp1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        //  假设要完成一个学校的校园管理信息系统，在员工管理系统中有不同的人员信息，包括学生信息、教师信息等。
        //为学生信息、教师信息创建两个类，并在两个类中分别定义属性和方法，在学生类中定义编号（Id）、姓名（Name）、性别（Sex）
        //、身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）7 个属性，
        //并定义一个方法在控制台输出这些属性的值。
        //用同样的方法创建教师信息类（Teacher），属性包括编号（Id）、姓名（Name）,性别 （Sex）、身份证号（Cardid）
        //、联系方式（Tel）、职称（Title）、工资号（Wageno），并将上 述属性输岀到控制台。
        //将 Student 类和 Teacher 类中共有的 属性抽取出来定义为一个类Person
        static void Main(string[] args)
        {
            Student stu = new Student();
            stu.Id = 123;
            stu.Name = "张三";
            stu.Sex = "男";
            stu.Cardid = 123456;
            stu.Tel = 1230000;
            stu.Major = "软件技术";
            stu.Grade = "大一";
            stu.output();
            stu.output1();

            Console.WriteLine();

            Teacher Tea = new Teacher();
            Tea.Id = 456;
            Tea.Name = "李四";
            Tea.Sex = "女";
            Tea.Cardid = 789456;
            Tea.Tel = 456000;
            Tea.Title = "专业课老师";
            Tea.Wageno = "G123456";
            stu.output();
            Tea.output2();


        }
    }
}