﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("输入圆的半径");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("圆的面积为：" + a * a * 3.14);
        }
    }
}
