﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class2
    {
        //        定义一个员工类，存放用户的工号、姓名和部门信息；
        public int number;//工号
        public String name;//用户名
        public string department; //部门信息


        public Class2(int number, String name, string department)
        {
            this.number = number;
            this.name = name;
            this.department = department;
        }
        public void giao()//定义一个方法输出
        {
            Console.WriteLine("当前用户对象的账号是{0},用户名是{1},密码是{2}", number, name, department);
        }
        //定义两个构造函数，一个是无参；
        //一个有参构造函数，对类的属性进行初始化。

    }
    class Student
    {
        public int id;
        public string name;
        public int age;
        public Student()
        {
        }
        public Student(int id, string name, int age)
        {
            this.id = id;
            this.name = name;
            this.age = age;
        }
        public void fangfa1()
        {
            Console.WriteLine("我的学号是{0}，我的名字叫{1}，我的年龄是{2}", this.id, this.name, this.age);
        }
    }
    class SumUtils
    {
        //        创建一个名为计算工具类 SumUtils，在类中分别定义：
        //计算两个整数相加、
        //两个小数相加、 
        //两个字符串相加、
        //以及从 1 到指定整数的和的方法。
        //在 Main 方法中分别调用定义好的方法。

        //根据题目要求，分别定义 3 个带两个参数的方法，以及一个带一个整型参数的方法，
        //四个方法名相同。
        public int sum(int a, int b)
        {
            return a + b;
        }
        public double sum(double a, double b)
        {
            return a + b;
        }
        public string sum(string a, string b)
        {
            return a + b;
        }
        public int sum(int a)
        {
            int sum = 0;
            for (int i = 1; i < a; i++)
            {
                sum = i + sum;
            }
            return sum;
        }
    }
    class mianji
    {
        //        1. 定义一个计算图形面积的类，类中定义2个计算面积的方法（重载），分别计算圆面积和长方形面积两个方法。
        //提示：计算圆的面积传半径，计算长方形面积传长和宽。
        public double sum(double a)
        {
            return 3.14 * a * a;
        }
        public double sum(double a, double b)
        {
            return a * b;
        }

    }
}
