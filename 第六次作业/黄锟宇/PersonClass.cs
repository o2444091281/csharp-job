﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class PersonClass
    {   
        public string name;
        public string phone;
        public string address;
        public void output()
        {
            Console.WriteLine("我的姓名是{0}，电话是{1}，地址是{2}", this.name, this.phone, this.address);
        }

        public void output(string name)
        {
            Console.WriteLine("我的姓名是{0}，电话是{1}，地址是{2}", this.name, this.phone, this.address);
        }
    }
}