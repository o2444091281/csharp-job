﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            //1、定义一个方法，求一个整数数组中的最大值，最小值，和，
            //平均数。如果是一个方法只能有一个返回值，那只能每一个都得定义一个方法来实现，不过有了ref和out这实现起来就方便多了。
            //参考步骤：
            //定义一个一维数组，数组中存放一些数据（控制台输入）。
            //定义几个变量：max、min、sum、avg

            //定义一个方法，方法返回数组元素之和,方法形参有max min avg。

            //请用ref和out各做一遍，并在注释中，描述他们的区别。
            int[] arr = new int[5];
            Console.WriteLine("输入5个数");
            arr[0] = int.Parse(Console.ReadLine());
            arr[1] = int.Parse(Console.ReadLine());
            arr[2] = int.Parse(Console.ReadLine());
            arr[3] = int.Parse(Console.ReadLine());
            arr[4] = int.Parse(Console.ReadLine());
            int max;
            int min;
            int sum;
            int avg;
            calculate(arr, out max, out min, out sum, out avg);
            Console.WriteLine("最大" + max);
            Console.WriteLine("最小" + min);
            Console.WriteLine("和" + sum);
            Console.WriteLine("平均数" + avg);
        }
        //public static void calculate(int[] arr,ref int max, ref int min, ref int sum, ref int avg)
        //{
        //    max = arr[0];
        //    min = arr[0];
        //    sum = 0;

        //    foreach (var i in arr)
        //    {
        //        if (i>max)
        //        {
        //            max = i;
        //        }
        //        if (i <min)
        //        {
        //            min = i;
        //        }
        //        sum = sum + i;
        //    }
        //    avg = sum / 5;
        //}      ref版本
        public static void calculate(int[] arr, out int max, out int min, out int sum, out int avg)
        {
            max = arr[0];
            min = arr[0];
            sum = 0;

            foreach (var i in arr)
            {
                if (i > max)
                {
                    max = i;
                }
                if (i < min)
                {
                    min = i;
                }
                sum = sum + i;
            }
            avg = sum / 5;
        }
    }
}