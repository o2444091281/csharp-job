﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work05._12;

namespace ConsoleApp1
{
    class Program
    {
        

       
        static void Main(string[] args)
        {

            Information student = new Information();

            student.userName = "adamin";
            student.Password = "admin";
            student.PasswordAttributes = "英文字母";

            student.call();
        }
        
    


        static void Square()
        {
            //用户输入正方形边长，用*打印出实心正方形。
            Console.WriteLine("请输入正方形的边长数：");
            int a = int.Parse(Console.ReadLine());
            string[,] arr = new string[a, a];
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    arr[i, j] = "*";
                }
            }
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < a; j++)
                {
                    Console.Write(arr[i, j]);

                }
            }

        }

        //九九乘法表
        static void nine()
        {
            string[,] arr = new string[9, 9];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    arr[i, j] = (i + 1) + "*" + (j + 1) + "=" + (i + 1) * (i + 1) + "\t";
                }
            }

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write(arr[i, j]);

                }
            }

        }
        //static void nine2()
        //{
        //    int[][] arr2= new int[9][]; 
        //    for (int i = 0; i < arr2.GetLength(0); i++)
        //    {
        //        for (int j = 0; j <= i; j++)
        //        {
        //            arr2[i, j] = (i + 1) + "*" + (j + 1) + "=" + (i + 1) * (i + 1) + "\t";
        //        }
        //    }

        //    for (int i = 0; i < arr2.GetLength(0); i++)
        //    {
        //        Console.WriteLine();
        //        for (int j = 0; j < arr2.GetLength(1); j++)
        //        {
        //            Console.Write(arr2[i, j]);

        //        }
        //    }

        //}

        static void HollowSqune()
        {
            //用户输入正方形边长，用*打印出空心正方形。
            Console.WriteLine("请输入空心正方形的边长数：");
            int a = int.Parse(Console.ReadLine());
            string[,] arr = new string[a, a];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (i == 0 | i == arr.GetLength(0) - 1)
                    {
                        arr[i, j] = "*";
                    }
                    else
                    {
                        if (j == 0 || j == arr.GetLength(1) - 1)
                        {
                            arr[i, j] = "*";
                        }
                        else
                        {
                            arr[i, j] = " ";
                        }
                    }
                    Console.Write(arr[i, j]);

                }
                Console.WriteLine( );
            }
        }



        //菱形
        static void rhombus()
        {
            Console.WriteLine("请输入菱形的边长数：");
            int a = int.Parse(Console.ReadLine());
            string[,] arr = new string[a, a];
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    arr[i, j] = "*";
                }
            }
        }

        //在 Main 方法中创建一个字符串类型的数组，并存入 5 个值，然后将数组中下标是偶数的元素输出。
        static void index()
        {
            int[] arr = { 1, 50, 3, 45, 5 };
            int[] a = new int[5];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                if (arr[i] % 2 == 0)
                {
                    a[i] = arr[i];
                    Console.WriteLine(a[i]);
                }
            }
        }

        //如果一个数组保存元素是有序的（从大到小），
        //向这个数组中插入一个数，使得插入后的数组元素仍然保持有序。
        static void sort()
        {
            int[] arr = { 100, 80, 54, 50, 8 };
            int[] arr1 = new int[arr.Length + 1];
            Console.WriteLine("原数组：100, 80, 54, 50, 8");

            for (int i = 0; i < 5; i++)
            {
                arr1[i] = arr[i];
            }
            Console.WriteLine("这个数组中插入一个数");
            arr1[arr1.Length - 1] = int.Parse(Console.ReadLine());


            for (int i = 0; i < arr1.GetLength(0); i++)
            {
                for (int j = 0; j < arr1.GetLength(0) - 1; j++)
                {
                    if (arr1[j] < arr1[j + 1])
                    {
                        int b = arr1[j];
                        arr1[j] = arr1[j + 1];
                        arr1[j + 1] = b;
                    }
                }
            }
            Console.Write("新数组：");
            foreach (var item in arr1)
            {

                Console.Write(item + " ");
            }
        }
    }
}
