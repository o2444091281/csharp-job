﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class HeroInherit
    {
        public string RoleName { get; set; }
        public string HeroName { get; set; }
        public string Background { get; set; }
        public string ATK { get; set; }
        public string DEF { get; set; }
        public string Speed { get; set; }

        public string FirstSkill { get; set; }
        public string SecondSkill { get; set; }
        public string ThirdSkill { get; set; }


        public void output()
        {
            Console.WriteLine("名字：" + RoleName);
            Console.WriteLine("英雄姓名：" + HeroName);
            Console.WriteLine("背景：" + Background);
            Console.WriteLine("攻击力：" + ATK);
            Console.WriteLine("联系方式：" + DEF);
            Console.WriteLine("速度：" + Speed);
            Console.WriteLine("第一技能："+ FirstSkill);
            Console.WriteLine("第二技能：" + SecondSkill);
            Console.WriteLine("第三技能：" + ThirdSkill);
        }
    }
}