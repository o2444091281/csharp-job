﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work05._06_01
{
    class Program
    {
        static void Main(string[] args)
        {
            const float π = 3.14f;
            Console.WriteLine("请输入圆的半径：");
            int r = int.Parse(Console.ReadLine());
            Console.WriteLine("圆的面积为" + π * r * r);
        }
    }
}
