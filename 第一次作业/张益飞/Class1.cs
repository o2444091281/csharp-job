﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class01
{
    class Class1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个半径值:");
            double r= Convert.ToDouble(Console.ReadLine());
            double s = 3.14 * r * r;
            Console.WriteLine("圆的面积是"+s);
            Console.ReadLine();
        }
    }
}
