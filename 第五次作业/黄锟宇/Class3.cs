﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Static
    {
        static void Main(string[] args)
        {
            //        1、写一个工具类StringUtil，在里面定义一个静态方法，用来判断字符串是否为空。
            //        如果字符串是null，或者字符串是空""，或者字符串是N个空格"  "，那这个方法返回true，否则返回false
            //          然后在主类中（有Main方法的类）调用测试。

            //          2、写一个工具类ArrayUtil，在里面定义一个静态方法，用来判断数组是否为空。
            //          如果数组是null，或者数组长度为0，那此方法返回true，否则返回false
            //          然后在主类中（有Main方法的类）调用测试。
            //Console.WriteLine("输入一个字符串");
            //Console.WriteLine(StringUtil.judeg(Console.ReadLine()));

            //int[] arr = new int[5];

            //Console.WriteLine(ArrayUtil.judeg(arr));


            //            提示：
            //定义一个学生类（有哪些字段属性？修饰符是什么？自己考量定义，至少学生姓名要吧），定义一个有参构造方法用来初始化学生姓名。学生类中定义一个成员方法，用来输出学生的信息。

            //Main方法中，创建5个学生对象，每个学生执行输出学生信息的方法。
            //学生的老师变更，
            //再执行每个学生执行输出学生信息的方法。



            Stutent.teacher = "唐僧";
            Stutent stu = new Stutent("太上老君");

            Stutent stu1 = new Stutent("玉皇大帝");

            Stutent stu2 = new Stutent("卷帘大将");

            stu.say();
            stu1.say();
            stu2.say();
            Stutent.teacher = "溥仪老子";
            stu.say();
            stu1.say();
            stu2.say();
        }
    }
}
