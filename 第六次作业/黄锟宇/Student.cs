﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student: Inherit
    {
        //在学生类中定义编号（Id）、姓名（Name）、性别（Sex）
        //身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）7 个属性

        //public int Id;
        //public string Name;
        //public string Sex;
        //public int Cardid;
        //public int Tel;
        //public string Major;
        //public string Grade;

        public string Major { get; set; }
        public string Grade { get; set; }

        //public Student()
        //{
        //    this.Id = Id;
        //    this.Name = Name;
        //    this.Sex = Sex;
        //    this.Cardid = Cardid;
        //    this.Tel = Tel;
        //    this.Major = Major;
        //    this.Grade = Grade;
        //}

        public void output1()
        {
            Console.WriteLine("专业：" + Major);
            Console.WriteLine("年级：" + Grade);
        }

        //public void output1()
        //{
        //    Console.WriteLine("我的编号是{0},姓名是{1}+,性别是{2},身份证号是{3},联系方式是{4},专业是{5},年纪是{6}"
        //        ,this.Id,this.Name,this.Sex,this.Cardid,this.Tel,this.Major,this.Grade);
        //}
       
    }
}