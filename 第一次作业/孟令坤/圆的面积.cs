﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//要求用户输入半径的值，打印出以此值为半径的圆的面积
//求圆的面积的公式：πr²。
//π取3.14。
//r由用户输入并存入一个变量中，此变量用double比较合适，因为用户可能会输入小数。




namespace Dream
{
    class Program
    {
        static void Main(string[] args)
        {
            const float l = 3.14f;
            Console.WriteLine("请输入圆的半径");
            int r = int.Parse(Console.ReadLine());
            Console.WriteLine(l*r*r) ;
            Console.ReadKey();
        }
    }
}
