﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work05._12
{
    class Information
    {
        //定义一个用户类，存放用户的账号、用户名和密码属性；
   
        public string userName { get; set; }
        public string Password { get; set; }
        public string PasswordAttributes { get; set; }

        public void call()
        {
            Console.WriteLine("我的账号是{0}，我的密码是{1},我的密码属性是{2}", this.userName, this.Password, this.PasswordAttributes);
        }
    }
}