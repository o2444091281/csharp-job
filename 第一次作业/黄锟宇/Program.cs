﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("Hellow World");
            Console.ReadKey();
            Console.WriteLine("请输入圆形的半径");
            double r = double.Parse(System.Console.ReadLine());
            const double π = 3.14;
            double area = π * r * r;
            Console.WriteLine("圆形的面积是" + area);
        }
    }
}
