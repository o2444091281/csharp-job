﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Program
    {
        enum state
        {
            新订单,
            已发货,
            已签收
        }
        enum Fruit
        {
            香蕉 = 5,
            苹果 = 6,
            西瓜 = 4,
            草莓 = 7,
            葡萄 = 9,
            哈密瓜 = 4,
            奇异果 = 3
        }
        enum occupation
        {
            战士 = 1,
            法师,
            精灵
        }
        static void Main(string[] args)
        {
            //﻿1、编写程序，定义一个枚举类型，表示订单的状态值：新订单、已发货、已签收、已评价；声明一个枚举类型表示某个订单的状态并将其值输出。


            //state s = state.已发货;
            //Console.WriteLine(s);


            // 2、有一款叫做“切水果”的游戏，里面有各种各样的水果，
            //A、现在请定义一个叫做“Fruit”的枚举类型，里面有：香蕉、苹果、西瓜、草莓、葡萄、哈密瓜、奇异果。
            //B、然后定义一个输出水果分数的方法，参数类型就是这个水果枚举，方法中根据不同的枚举值，输出水果对应的分数
            //（可以参考切水果游戏）。注：水果形状越小，切到的时候分数就越高。

            //FruitGrade();



            //            3、RPG游戏中，通常有不同的职业，比如“战士”、“法师”、“精灵”等等职业，
            //A、现在请定义一个游戏职业的枚举。
            //B、然后定一个输出职业技能的方法，根据传入的职业枚举的值来输出，
            //战士的技能：碎石打击、烈焰锚钩、战斗咆哮
            //法师的技能：巨浪冲击、元素突击、复仇杀戮
            //精灵的技能：减速陷阱、能量浪潮、旋风剑舞
            Console.WriteLine("请输入想要查看的职业：1、战士：2、法师：3、精灵");

            Program p = new Program();

            Console.WriteLine(p.Skill(int.Parse(Console.ReadLine())));




        }
        public String Skill(int a)
        {
            string i = "";
            if (a == 1)
            {
                i = "战士的技能：碎石打击、烈焰锚钩、战斗咆哮";
            }
            else if (a == 2)
            {
                i = "法师的技能：巨浪冲击、元素突击、复仇杀戮";
            }
            else if (a == 3)
            {
                i = "精灵的技能：减速陷阱、能量浪潮、旋风剑舞";
            }
            return i;
        }
    }
    //static void FruitGrade()
    //{

    //    Console.WriteLine("哈密瓜的分数" + (int)Fruit.哈密瓜);
    //    Console.WriteLine("奇异果的分数" + (int)Fruit.奇异果);
    //    Console.WriteLine("苹果的分数" + (int)Fruit.苹果);
    //    Console.WriteLine("草莓的分数" + (int)Fruit.草莓);
    //    Console.WriteLine("葡萄的分数" + (int)Fruit.葡萄);
    //    Console.WriteLine("西瓜的分数" + (int)Fruit.西瓜);
    //    Console.WriteLine("香蕉的分数"+(int)Fruit.香蕉);
    //}


}