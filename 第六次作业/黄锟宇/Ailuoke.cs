﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Ailuoke:HeroInherit
    {
        static void Main(string[] args)
        {
            Ailuoke ailuoke = new Ailuoke();
            ailuoke.RoleName = "德玛西亚";
            ailuoke.HeroName = "埃洛克";
            ailuoke.Background = "埃洛克是一名来自末日边境的勇士";
            ailuoke.ATK = "80";
            ailuoke.DEF = "70";
            ailuoke.Speed = "50";
            ailuoke.FirstSkill = "碎石打击";
            ailuoke.SecondSkill = "烈焰抛锚";
            ailuoke.ThirdSkill = "战斗咆哮";
            ailuoke.output();

            Console.WriteLine();

           
            ailuoke.RoleName = "艾欧尼亚";
            ailuoke.HeroName = "泰拉";
            ailuoke.Background = "泰拉是为了复仇而来的勇者";
            ailuoke.ATK = "80";
            ailuoke.DEF = "65";
            ailuoke.Speed = "63";
            ailuoke.FirstSkill = "巨浪冲击";
            ailuoke.SecondSkill = "元素突击";
            ailuoke.ThirdSkill = "复仇杀戮";
            ailuoke.output();


            Console.WriteLine();


            ailuoke.RoleName = "诺克萨斯";
            ailuoke.HeroName = "卢卡斯";
            ailuoke.Background = "卢卡斯是一名彬彬有礼的剑客";
            ailuoke.ATK = "85";
            ailuoke.DEF = "65";
            ailuoke.Speed = "70";
            ailuoke.FirstSkill = "减速陷阱";
            ailuoke.SecondSkill = "能量浪潮";
            ailuoke.ThirdSkill = "旋风剑舞";
            ailuoke.output();

            Console.WriteLine();

            ailuoke.RoleName = "巨神峰";
            ailuoke.HeroName = "洛菲";
            ailuoke.Background = "洛菲是一名攻击迅猛";
            ailuoke.ATK = "75";
            ailuoke.DEF = "65";
            ailuoke.Speed = "80";
            ailuoke.FirstSkill = "能量精灵";
            ailuoke.SecondSkill = "暗影传送";
            ailuoke.ThirdSkill = "时空迸裂";
            ailuoke.output();


        }
    }
}