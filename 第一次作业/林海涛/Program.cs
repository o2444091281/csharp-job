﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    //用户输入圆的半径
    //结果输出圆的面积
    class Program
    {
        static void Main(string[] args)
        {
            double Π = 3.14;
            double area;

            Console.WriteLine("请输入圆的半径");
            double r = double.Parse(Console.ReadLine());
            area = (r * r) * Π;
            Console.WriteLine("圆的面积为"+ area );
        }
    }
}
