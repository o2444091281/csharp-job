﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Class2 arr = new Class2(123, "张三", "软件部");
            arr.giao(); //然后在主方法调用输出

            Student stu = new Student(01, "张三", 18);
            stu.fangfa1();

            SumUtils su = new SumUtils();
            Console.WriteLine("两个整数的和为：" + su.sum(2, 3));
            Console.WriteLine("两个小数的和为：" + su.sum(1.1, 2.2));
            Console.WriteLine("两个字符串的和为：" + su.sum("什么", "牛马"));
            Console.WriteLine("从一到五的和为：" + su.sum(10));

            mianji mian = new mianji();
            Console.WriteLine("圆的面积=" + mian.sum(5));
            Console.WriteLine("长方形的面积=" + mian.sum(3, 6));
        }

    }
}